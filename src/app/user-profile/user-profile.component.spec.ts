import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { UserProfileComponent } from './user-profile.component';
import { ToastComponent } from '../toast/toast.component';
import { LoginComponent } from '../login/login.component';
import { HeaderComponent } from '../header/header.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { StorageService } from '../../services/storage.service';
import { EventsService } from '../../services/events.service';
import { UtilService } from '../../services/util.service';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { TokenService } from '../../services/token.service';
import { UserService } from '../../services/user.service';
import { ToastService } from '../../services/toast.service';
import { TopicService } from '../../services/topic.service';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { SignupComponent } from '../signup/signup.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ToastyModule } from 'ng2-toasty';

import { Observable } from 'rxjs';


describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent,
        ToastComponent,
        LoginComponent,
        EditProfileComponent,
        SignupComponent,
        UserProfileComponent],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        AppRoutingModule,
        FormsModule,
        ToastyModule.forRoot(),
        MatCheckboxModule
      ],
      providers: [
        EventsService,
        StorageService,
        { provide: UserService, useClass: MockUserService },
        // { provide: TopicService, useClass: MockTopicService },
        ToastService,
        UtilService,
        // { provide: Router, useClass: RouterModule },
        { provide: Router, useClass: class { navigate = jasmine.createSpy("navigate") } },
        { provide: ActivatedRoute, useClass: MockActivatedRoute }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render user profile', fakeAsync(() => {
    fixture.detectChanges();
    let service = new MockUserService();
    let utilsevice = new UtilService();


    service.get(2).subscribe((user: any) => {
      component.user = user.data;
    });

    service.getusertopics(2).subscribe((topics: any) => {
      let userTopics = [];
      userTopics = utilsevice.parseUserTopics(topics.data);
      component.user['topics'] = userTopics.sort(component.compare);
    });

    tick();

    fixture.detectChanges();
    expect(component.user.userID).toEqual(2);
  }));


  class MockRestService {

  }

  class MockUserService {
    data = {
      "userID": 2,
      "firstName": "Prashant",
      "lastName": "Verma",
      "email": "pra0408@gmail.com",
      "designation": "SDE3",
      "description": "I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase",
      "updatedAt": "2018-04-01T08:13:42.603339Z",
      "createdAt": "2018-04-01T08:13:42.603622Z"
    }

    get(id) {
      return new Observable(observer => {
        let data = {};
        data['data'] = {
          "userID": 2,
          "firstName": "Prashant",
          "lastName": "Verma",
          "email": "pra0408@gmail.com",
          "designation": "SDE3",
          "description": "I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase",
          "updatedAt": "2018-04-01T08:13:42.603339Z",
          "createdAt": "2018-04-01T08:13:42.603622Z"
        }
        observer.next(data);
      });
    }
    getusertopics(id) {
      return new Observable(observer => {
        let data = {};
        data['data'] = [
          {
            "usertopicID": 103,
            "status": true,
            "topicID": {
              "topicID": 5,
              "name": "Food",
              "groupID": 1
            }
          },
          {
            "usertopicID": 104,
            "status": true,
            "topicID": {
              "topicID": 8,
              "name": "Javascript",
              "groupID": 2
            }
          },
          {
            "usertopicID": 105,
            "status": true,
            "topicID": {
              "topicID": 9,
              "name": "Python",
              "groupID": 2
            }
          },
          {
            "usertopicID": 106,
            "status": true,
            "topicID": {
              "topicID": 11,
              "name": "Angular",
              "groupID": 2
            }
          },
          {
            "usertopicID": 114,
            "status": true,
            "topicID": {
              "topicID": 4,
              "name": "Film",
              "groupID": 1
            }
          },
          {
            "usertopicID": 115,
            "status": true,
            "topicID": {
              "topicID": 10,
              "name": "React",
              "groupID": 2
            }
          }
        ]

        observer.next(data);


      });
    }
  }
  class MockActivatedRoute {

    params = {
      subscribe: jasmine.createSpy('subscribe')
        .and
        .returnValue(Observable.of(<any>{ id: 2 }))
    }

  }
})
