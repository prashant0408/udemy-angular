import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UtilService } from '../../services/util.service';
import { StorageService } from '../../services/storage.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'udemy-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user: any = {
    'topics': []
  };
  loggedUser: any;
  showEdit: any = false;
  constructor(
    private userservice: UserService,
    private utilsevice: UtilService,
    private storage: StorageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      this.getUserDetails(id);
      this.getUserTopics(id);
      this.storage.get('user').then((user: any) => {
        this.loggedUser = JSON.parse(user) || {};
        if (this.loggedUser && this.loggedUser.userID == id) {
          this.showEdit = true;
        }


      });

    });

  }

  getUserDetails(id) {
    this.userservice.get(id).subscribe((user) => {
      if (user) {
        console.log(user);
        this.user = user.data;
      }
    })
  }

  getUserTopics(id) {
    this.userservice.getusertopics(id).subscribe((topics) => {
      if (topics) {
        console.log(topics);
        this.parseUserTopics(topics.data);
      }
    })
  }

  parseUserTopics(data) {
    let userTopics = [];
    userTopics = this.utilsevice.parseUserTopics(data);
    this.user['topics'] = userTopics.sort(this.compare);
    console.log('-------LoogedUserProfile-------');
    console.log(this.loggedUser.topics);
    console.log('-------LoogedUserProfile-------');
    if (this.loggedUser && this.loggedUser.topics) {
      this.user.topics = this.compareSimilar(this.user.topics, this.loggedUser.topics);
    }
    console.log('-------UserProfile-------');
    console.log(this.user.topics);
    console.log('-------UserProfile-------');
  }

  compare(a, b) {
    if (a.name < b.name)
      return -1;
    if (a.name > b.name)
      return 1;
    return 0;
  }

  openEditProfile() {
    this.router.navigate(['/edit']);
  }

  compareSimilar(data1, data2) {
    let similar = [];
    let diff = [];
    let loggedTopicMap = {};
    for (let j = 0; j < data2.length; j++) {
      let id = data2[j]['topicID'];
      loggedTopicMap[id] = data2[j];

    }
    for (let i = 0; i < data1.length; i++) {
      let id = data1[i]['topicID'];
      if (id in loggedTopicMap) {
        similar.push(data1[i]);
      }
      else {
        diff.push(data1[i]);
      }
    }
    return similar.concat(diff);
  }


}
