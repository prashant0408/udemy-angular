import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StorageService } from '../services/storage.service';
import { EventsService } from '../services/events.service';
import { UtilService } from '../services/util.service';
import { RestService } from '../services/rest.service';
import { ConfigService } from '../services/config.service';
import { TokenService } from '../services/token.service';
import { UserService } from '../services/user.service';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SignupComponent } from './signup/signup.component';
import { ToastComponent } from './toast/toast.component';
import { ToastyModule } from 'ng2-toasty';
import { ToastService } from '../services/toast.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { TopicService } from '../services/topic.service';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { UserProfileComponent } from './user-profile/user-profile.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SignupComponent,
    ToastComponent,
    EditProfileComponent,
    UserProfileComponent
  ],
  entryComponents: [
      ToastComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ToastyModule.forRoot(),
    MatCheckboxModule
  ],
  providers: [
    EventsService,
    StorageService,
    RestService,
    ConfigService,
    TokenService,
    UserService,
    ToastService,
    TopicService,
    UtilService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
