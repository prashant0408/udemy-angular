import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'udemy-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loggedUser: any = {};
  constructor(
    private storage: StorageService,
    private router: Router,
    private userservice: UserService
  ) { }

  ngOnInit() {
    this.storage.get('user').then((user: any) => {
      this.loggedUser = JSON.parse(user) || {};
      console.log(this.loggedUser)
    });
  }

  openLogin() {
    this.router.navigate(['/login']);
  }

  logout() {
    this.userservice.logout((status) => {
      this.openLogin();
    });

  }


}
