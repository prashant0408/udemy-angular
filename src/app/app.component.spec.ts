import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ToastComponent } from './toast/toast.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { StorageService } from '../services/storage.service';
import { EventsService } from '../services/events.service';
import { UtilService } from '../services/util.service';
import { RestService } from '../services/rest.service';
import { ConfigService } from '../services/config.service';
import { TokenService } from '../services/token.service';
import { UserService } from '../services/user.service';
import { ToastService } from '../services/toast.service';
import { TopicService } from '../services/topic.service';
import { Router, RouterModule } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SignupComponent } from './signup/signup.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ToastyModule } from 'ng2-toasty';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        AppComponent,
        ToastComponent,
        LoginComponent,
        HeaderComponent,
        SignupComponent,
        EditProfileComponent,
        UserProfileComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        AppRoutingModule,
        FormsModule,
        ToastyModule.forRoot(),
        MatCheckboxModule
      ],
      providers: [
        { provide: Http, useClass: class { http = jasmine.createSpy("http") } },
        EventsService,
        StorageService,
        RestService,
        ConfigService,
        TokenService,
        UserService,
        ToastService,
        TopicService,
        UtilService,
        // { provide: Router, useClass: RouterModule },
        { provide: Router, useClass: class { navigate = jasmine.createSpy("navigate") } }

      ],
      // imports: [AppRoutingModule]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});

class MockRestService {
  initializeTokens() {

  }
}
