import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'udemy-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupData: any = {
    email: '',
    password: '',
    confirmpassword: '',
  };
  isDisabled = true;
  isPasswordInValid = true;
  formErrors: any;
  constructor(
    private userservice: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  createUser() {
    this.userservice.signup(this.signupData).subscribe((data) => {
      console.log(data);
      this.openlogin();
    }, (err) => {
      this.formErrors = err.errors;
    });
  }

  validatePassword(event: any = '') {
    if (this.signupData.password != '' && this.signupData.confirmpassword != '' && this.signupData.password == this.signupData.confirmpassword) {
      this.isPasswordInValid = false
      return true;
    }
    this.isPasswordInValid = true;
    return false;
  }

  validateEmail(event) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(event) == true) {
      this.isDisabled = false
      return true;
    }
    this.isDisabled = true
    return false;
  }
  openlogin() {
    this.router.navigate(['/login']);
  }

}
