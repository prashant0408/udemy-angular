import { Component, ViewContainerRef, HostListener } from '@angular/core';
import { EventsService } from '../services/events.service';
import { UserService } from '../services/user.service';
import { ToastService } from '../services/toast.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { RestService } from '../services/rest.service';
// import { LoaderService } from '../services/loader.service'
import { environment } from '../environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  currentUser: any = {};
  componentRefs: any = [];
  currentUrl: any;
  showTooltip = true;

  constructor(
    public events: EventsService,
    public toast: ToastService,
    public router: Router,
    public rest: RestService,
    public user: UserService,
    private viewContainerRef: ViewContainerRef) {

    this.currentUrl = router.url;
    this.initializeApp();

  }


  initializeApp() {
    this.events.subscribe('toast:error', toast => {
      this.toast.addToast(toast, 'error');
    });

    this.events.subscribe('toast:success', toast => {
      this.toast.addToast(toast, 'success');
    });

    this.events.subscribe('toast:warning', toast => {
      this.toast.addToast(toast, 'warning');
    });

    this.events.subscribe('logout', (param) => {
      this.logout();
    });

    this.events.subscribe('error', error => {
      const err = error;

      if (err.status === 401) {
        if (err.errors && err.errors.hasOwnProperty('accessToken')) {
          this.events.publish('toast:error', err.errors.accessToken[0]);
          // this.logout();
        }
        this.logout();
      }
      if (err.status === 500) {
        this.events.publish('toast:error', '500 Something went wrong!');
      }

      if (err.status !== 403 && err.status !== 400 && err.status !== 401 && err.status !== 500) {
        this.events.publish('toast:error', 'Please Check Internet Connection');
      }
    });


    this.user.initialize().then(resp => {
      if (!this.rest.isLoggedIn()) {
        const url = window.location.href.split('/');
        if (url[url.length - 2] === 'user') {
          const routeurl = '/' + url[url.length - 2] + '/' + url[url.length - 1];
          this.router.navigate([routeurl]);
        }
        else {
          this.router.navigate(['/login']);
        }

      }
      else {
        this.router.initialNavigation();
      }
    });

  }

  logout() {
    this.user.logout((status) => {
      this.router.navigate(['/login']);
    });

  }
}
