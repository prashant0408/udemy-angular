import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { StorageService } from '../../services/storage.service';
import { Router, NavigationEnd, } from '@angular/router';
import { UtilService } from '../../services/util.service';

@Component({
  selector: 'udemy-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData: any = {
    email: '',
    password: '',
  };
  formErrors: any;
  isDisabled = true;
  isPasswordInValid = true;
  user: any = {};
  constructor(
    private userservice: UserService,
    private utilsevice: UtilService,
    private storage: StorageService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  login() {
    this.userservice.login(this.loginData).subscribe((user) => {
      this.user = user.data;
      this.getUserTopics();
    }, (err) => {
      console.log(err);
      this.formErrors = err.errors;
    })
  }

  validatePassword(event) {
    if (this.loginData.password !== '' && this.loginData.password.length > 5) {
      this.isPasswordInValid = false
      return
    }
    this.isPasswordInValid = true;
    return
  }


  validateEmail(event) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(event) == true) {
      this.isDisabled = false
    }
  }

  getUserTopics() {
    this.userservice.getusertopics(this.user.userID).subscribe((usertopics) => {
      let mytopics = usertopics.data;

      this.user['topics'] = [];
      this.user.topics = this.utilsevice.parseUserTopics(mytopics);
      console.log('-------Login-------');
      console.log(this.user.topics);
      console.log('-------Login-------');
      this.storage.set('user', JSON.stringify(this.user)).then((user: any) => {
        this.router.navigate(['/edit']);
      });

    })
  }

  opensignup() {
    this.router.navigate(['/signup']);
  }


}
