import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { TopicService } from '../../services/topic.service';
import { StorageService } from '../../services/storage.service';
import { Router, NavigationEnd } from '@angular/router';
import { UtilService } from '../../services/util.service';
import * as _ from 'lodash';


@Component({
  selector: 'udemy-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  user: any = {
  }


  formData: any = {};
  formErrors: any = {};
  topics: any;

  checked = false;
  indeterminate = false;
  align = 'start';
  disabled = false;
  selectedTopics = [];
  preselectedTopicIDs = {}
  unselectedTopics = [];
  newSelectedTopics = [];

  constructor(
    private userservice: UserService,
    private topicsevice: TopicService,
    private storage: StorageService,
    private utilsevice: UtilService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.storage.get('user').then((user: any) => {
      this.user = JSON.parse(user) || {};
      this.getUserTopics(this.user.userID);
      console.log(this.user);

    })

  }

  getUserTopics(id) {
    this.userservice.getusertopics(id).subscribe((topics) => {
      if (topics) {
        console.log(topics);
        this.parseSelectedTopics(topics.data);
        this.getAllTopics();
      }
    })
  }

  parseSelectedTopics(topics) {
    let mytopics = topics;
    for (let i = 0; i < mytopics.length; i++) {
      let topic = mytopics[i]['topicID']
      let status = mytopics[i].status;
      let usertopicID = mytopics[i].usertopicID
      topic['status'] = status;
      topic['usertopicID'] = usertopicID;
      this.preselectedTopicIDs[topic.topicID] = topic;
      this.selectedTopics.push(topic);
    }

  }

  getAllTopics() {
    this.topicsevice.get(null).subscribe((topics) => {
      console.log(topics)
      this.topics = topics.data;
      this.topics = this.topics.filter(topic => {
        if (topic.topicID in this.preselectedTopicIDs) {
          topic['status'] = true;
        }
        return topic
      })
    })
  }

  edit() {
    for (let key in this.user) {
      if (key == 'firstName' || key == 'lastName') {
        this.user['name'] = this.user.firstName + ' ' + this.user.lastName;
      }
    }
    this.newSelectedTopics = _.filter(this.topics, (topic: any) => {
      if (topic.status) {
        if (this.preselectedTopicIDs[topic.topicID]) {
          topic['usertopicID'] = this.preselectedTopicIDs[topic.topicID]['usertopicID'];
        }
        return topic;
      }
    });

    this.unselectedTopics = _.cloneDeep(_.differenceWith(this.selectedTopics, this.newSelectedTopics, _.isEqual));
    this.unselectedTopics = _.filter(this.unselectedTopics, (topic: any) => {
      topic.status = false;
      return topic;
    });


    console.log('-------newSelectedTopics-------');
    console.log(this.newSelectedTopics);
    console.log('-------newSelectedTopics-------');
    console.log('-------unselectedTopics-------');
    console.log(this.unselectedTopics);
    console.log('-------unselectedTopics-------');
    if (this.newSelectedTopics) {
      if (this.newSelectedTopics.length > 6) {
        this.formErrors['topics'] = 'Maximum 6 topics allowed';
        return;
      }
      this.saveTopics();
      console.log();
    }
    this.userservice.save(this.user).subscribe((data) => {
      if (data) {
        this.storage.set('user', JSON.stringify(this.user)).then((user: any) => {
        });
      }
    })
  }

  saveTopics() {
    let body = {
      userID: this.user.userID,
      topics: this.newSelectedTopics.concat(this.unselectedTopics)
    }
    this.userservice.savetopics(body).subscribe((topics) => {
      if (topics) {
        console.log(topics.data);
        for (let i = 0; i < topics.data.length; i++) {
          let topic = topics.data[i]['topicID'];
          topic.usertopicID = topics.data[i]['usertopicID'];
          this.preselectedTopicIDs[topic.topicID] = topic;
        }
        this.user['topics'] = this.newSelectedTopics;
        console.log('-------SaveTopics-------');
        console.log(this.user.topics);
        console.log('-------SaveTopics-------');
        this.storage.set('user', JSON.stringify(this.user)).then((user: any) => {
          this.router.navigate(['/user/' + this.user.userID]);
        });
      }
    })
  }

  validateName(ev) {

  }
  validatePos(ev) {

  }

  cancelEdit(id) {
    this.router.navigate(['/user/' + id]);
  }

}
