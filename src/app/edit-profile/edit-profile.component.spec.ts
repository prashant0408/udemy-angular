import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileComponent } from './edit-profile.component';
import { ToastComponent } from '../toast/toast.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { StorageService } from '../../services/storage.service';
import { EventsService } from '../../services/events.service';
import { UtilService } from '../../services/util.service';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { TokenService } from '../../services/token.service';
import { UserService } from '../../services/user.service';
import { ToastService } from '../../services/toast.service';
import { TopicService } from '../../services/topic.service';
import { Router, RouterModule } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LoginComponent } from '../login/login.component';
import { HeaderComponent } from '../header/header.component';
import { SignupComponent } from '../signup/signup.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ToastyModule } from 'ng2-toasty';
import { Observable } from 'rxjs';
describe('EditProfileComponent', () => {
  let component: EditProfileComponent;
  let fixture: ComponentFixture<EditProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [EditProfileComponent,
        ToastComponent,
        LoginComponent,
        HeaderComponent,
        SignupComponent,
        UserProfileComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        AppRoutingModule,
        FormsModule,
        ToastyModule.forRoot(),
        MatCheckboxModule
      ],
      providers: [
        EventsService,
        StorageService,
        ConfigService,
        TokenService,
        { provide: UserService, useClass: MockUserService },
        ToastService,
        { provide: TopicService, useClass: MockTopicService },
        UtilService,
        // { provide: Router, useClass: RouterModule },
        { provide: Router, useClass: class { navigate = jasmine.createSpy("navigate") } }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class MockTopicService {
  get(id) {
    return new Observable(observer => {
      let data = {};
      data['data'] = [
        {
          "topicID": 1,
          "name": "Art",
          "groupID": 1
        },
        {
          "topicID": 2,
          "name": "Comics",
          "groupID": 1
        },
        {
          "topicID": 3,
          "name": "Culture",
          "groupID": 1
        },
        {
          "topicID": 4,
          "name": "Film",
          "groupID": 1
        },
        {
          "topicID": 5,
          "name": "Food",
          "groupID": 1
        },
        {
          "topicID": 6,
          "name": "Music",
          "groupID": 1
        },
        {
          "topicID": 7,
          "name": "Photography",
          "groupID": 1
        },
        {
          "topicID": 8,
          "name": "Javascript",
          "groupID": 2
        },
        {
          "topicID": 9,
          "name": "Python",
          "groupID": 2
        },
        {
          "topicID": 10,
          "name": "React",
          "groupID": 2
        },
        {
          "topicID": 11,
          "name": "Angular",
          "groupID": 2
        }
      ]
      observer.next(data);
    });
  }
}

class MockUserService {

  get(id) {
    return new Observable(observer => {
      let data = {};
      data['data'] = {
        "userID": 2,
        "firstName": "Prashant",
        "lastName": "Verma",
        "email": "pra0408@gmail.com",
        "designation": "SDE3",
        "description": "I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase I am a testcase",
        "updatedAt": "2018-04-01T08:13:42.603339Z",
        "createdAt": "2018-04-01T08:13:42.603622Z"
      }
      observer.next(data);
    });
  }
  getusertopics(id) {
    return new Observable(observer => {
      let data = {};
      data['data'] = [
        {
          "usertopicID": 103,
          "status": true,
          "topicID": {
            "topicID": 5,
            "name": "Food",
            "groupID": 1
          }
        },
        {
          "usertopicID": 104,
          "status": true,
          "topicID": {
            "topicID": 8,
            "name": "Javascript",
            "groupID": 2
          }
        },
        {
          "usertopicID": 105,
          "status": true,
          "topicID": {
            "topicID": 9,
            "name": "Python",
            "groupID": 2
          }
        },
        {
          "usertopicID": 106,
          "status": true,
          "topicID": {
            "topicID": 11,
            "name": "Angular",
            "groupID": 2
          }
        },
        {
          "usertopicID": 114,
          "status": true,
          "topicID": {
            "topicID": 4,
            "name": "Film",
            "groupID": 1
          }
        },
        {
          "usertopicID": 115,
          "status": true,
          "topicID": {
            "topicID": 10,
            "name": "React",
            "groupID": 2
          }
        }
      ]

      observer.next(data);


    });
  }
}
