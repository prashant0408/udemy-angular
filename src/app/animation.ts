import { animate, AnimationEntryMetadata, state, style, transition, trigger, keyframes } from '@angular/core';
export const toastAnimation: AnimationEntryMetadata =
  trigger('toastAnimation', [
    transition(':enter', [
      style({
        transform: 'translateY(150px)'
      }),
      animate('200ms cubic-bezier(0.215, 0.61, 0.355, 1)')
    ]),
    transition(':leave', [
      animate('200ms ease-out', style({ transform: 'translateY(150px)' }))
    ])
  ]);
