import { Component, OnInit, Input } from '@angular/core';
import { toastAnimation } from '../animation';
@Component({
  selector: 'udemy-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  animations: [toastAnimation],

  host: {
    '[style.position]': '"fixed"',
    '[style.z-index]': '"2"',
    '[style.background]': '"transparent"',
    '[style.height]': '"72px"',
    '[style.box-shadow]': '"0 0 6px 0 rgba(0, 0, 0, 0.2)"',
    '[style.left]': '"0"',
    '[style.bottom]': '"50px"',
    '[style.right]': '"0"',
    '[style.display]': '"flex"',
    '[style.justify-content]': '"center"',
    '[@toastAnimation]': "'true'"
  }
})
export class ToastComponent implements OnInit {

  @Input() message: string;
  @Input() type: string;
  @Input() viewController: any;

  constructor() {}

  ngOnInit() {}

}
