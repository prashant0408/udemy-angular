import { Injectable } from '@angular/core';
import { RestService } from './rest.service';

@Injectable()
export class TokenService {

  constructor(public rest: RestService) {}

  delete(id: string) {
    return this.rest.delete('token/' + id);
  }

}
