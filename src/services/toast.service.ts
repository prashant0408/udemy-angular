import { Injectable, ComponentFactoryResolver, ApplicationRef } from '@angular/core';
import { ToastComponent } from '../app/toast/toast.component'
import { environment } from '../environments/environment';

@Injectable()
export class ToastService {

  private viewContainerRef: any;
  private toastrs: any = [];
  private config: any = {
    duration: 2000,
    dismissOnTouch: true
  };

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef) {}

  setviewContainerRef() {
    this.viewContainerRef = this.viewContainerRef || this.appRef.components[0].instance.viewContainerRef;
  }


  addToast(message ? : string, type ? : string) {
    this.setviewContainerRef();
    let factory = this.componentFactoryResolver.resolveComponentFactory(ToastComponent);
    let ref = this.viewContainerRef.createComponent(factory);
    ref.instance['message'] = message;
    ref.instance['type'] = type;
    ref.instance['viewController'] = {
      dismiss: () => {
        if (this.config.dismissOnTouch) {
          ref.destroy();
        }
      }
    };

    this.toastrs.push(ref);

    ref.changeDetectorRef.detectChanges();

    setTimeout(() => {
      this.hide();
    }, this.config.duration);

    return ref;
  }

  hide() {
    let ref = this.toastrs.pop();
    if (ref) {
      ref.destroy();
    }
  }


}
