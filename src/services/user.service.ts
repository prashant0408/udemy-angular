import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { StorageService } from './storage.service';
import { EventsService } from './events.service';
import { TokenService } from './token.service';

@Injectable()
export class UserService {
  isLogoutProcessing: boolean = false;
  public userData: any = {};

  constructor(public rest: RestService,
    public storage: StorageService,
    public token: TokenService,
    public events: EventsService) {}

    get(userID:any){
      return this.rest.get('user/'+userID,0);
    }

    login(body: Object) {
      return this.rest.post('user/login', body).map(this.saveToStorage);
    }

    save(body:any){
      if(body.userID){
        return this.rest.put('user/'+body.userID, body);
      }

    }
    savetopics(body:any){
      if(body.userID){
        return this.rest.post('user/addtopics', body);
      }

    }
    getusertopics(userID:any){
        return this.rest.get('user/topics?userID='+userID,0);
    }

    signup(body:Object){
      return this.rest.post('user/signup', body);
    }

    initialize() {
    return new Promise((resolve, reject) => {
      this.rest.initializeTokens().then(resp => {
        this.storage.get('user').then((user: any) => {

          if (user) {
            this.userData = JSON.parse(user);
            resolve(JSON.parse(user));
          } else {
            resolve({});
            this.userData = {};
          }
        });

      });
    });
  }

  clean(cb) {
    this.storage.clear().then(() => {
      this.initialize();
      this.isLogoutProcessing = false;
      cb();
    });

  }

  logout(cb) {
    if (this.isLogoutProcessing) return cb(false);
    this.isLogoutProcessing = true;

    if (this.rest.isLoggedIn()) {
      this.token.delete(this.rest.accessToken).subscribe(data => {

        this.clean(() => {

          this.events.publish('toast:success', 'Logout Successful');
          cb(true);
        });
      }, err => {

        this.clean(() => {
          cb(true);
        });
      });
    } else {
      this.clean(() => {
        cb(true);
      })
    }
  }




  saveToStorage = (user) => {
    if (user) this.storage.set('user', JSON.stringify(user.data)).then(() => {
      this.initialize();
    });
    return user;
  }


}
