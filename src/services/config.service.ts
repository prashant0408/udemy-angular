import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class ConfigService {
  public apiUrl: string;
  public basePrefix: string;

  constructor(private http: Http) {
    this.apiUrl = environment.apiUrl;
    this.basePrefix = environment.basePrefix;
  }



}
