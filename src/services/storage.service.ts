import { Injectable } from '@angular/core';
import * as localforage from 'localforage';

@Injectable()
export class StorageService {

  constructor() {
    localforage.config({
      driver: localforage.LOCALSTORAGE, // Force WebSQL; same as using setDriver()
      name: 'myApp',
      version: 1.0,
      size: 4980736, // Size of database, in bytes. WebSQL-only for now.
      storeName: 'udemy', // Should be alphanumeric, with underscores.
      description: 'udemy localstorage'
    });
  }

  set(key, value) {
    return localforage.setItem(key, value);
  }

  get(key) {
    return localforage.getItem(key);
  }

  remove(key) {
    return localforage.removeItem(key);
  }

  clear() {
    return localforage.clear();
  }

}
