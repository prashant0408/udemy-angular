import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ConfigService } from './config.service';
import { StorageService } from './storage.service';
// import { ToastService } from './toast.service';
import { EventsService } from './events.service';

@Injectable()
export class RestService {

  public accessToken: any;
  public cache: any = {};

  constructor(public http: Http,
    public storage: StorageService,
    public config: ConfigService,
    public events: EventsService,
    // public toastService: ToastService,
  ) {
    }

  public get(partialUrl: string, cacheExpiry: number, showLoading: boolean = true): Observable < any > {
    if (cacheExpiry > 0 && this.cache[this.config.apiUrl + partialUrl]) return Observable.of(this.cache[this.config.apiUrl + partialUrl]);

    if (showLoading) this.events.publish('loader:show');

    let headers = new Headers({ 'method-override-header': 'GET', 'Access-Token': this.accessToken, });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.config.apiUrl + partialUrl, options).map(this.extractData).catch(this.handleError);
  }

  public post(partialUrl: string, params: any, showLoading: boolean = true): Observable < any > {
    if (showLoading) this.events.publish('loader:show');
    let body = JSON.stringify(params);
    let headers = new Headers({ 'Content-Type': 'application/json', 'method-override-header': 'POST', 'Access-Token': this.accessToken,  });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.config.apiUrl + partialUrl, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public put(partialUrl: string, params: any): Observable < any > {
    let body = JSON.stringify(params);
    let headers = new Headers({ 'Content-Type': 'application/json', 'method-override-header': 'PUT', 'Access-Token': this.accessToken,  });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.config.apiUrl + partialUrl, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public patch(partialUrl: string, params: any): Observable < any > {
    let body = JSON.stringify(params);
    let headers = new Headers({ 'Content-Type': 'application/json', 'method-override-header': 'PATCH', 'Access-Token': this.accessToken,  });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.config.apiUrl + partialUrl, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public delete(partialUrl: string): Observable < any > {
    let headers = new Headers({ 'Content-Type': 'application/json', 'method-override-header': 'DELETE', 'Access-Token': this.accessToken,  });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.config.apiUrl + partialUrl, null, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public getAllTokens() {
    return {
      'Access-Token': this.accessToken
    };
  }


  public initializeTokens() {

    return this.storage.get('accessToken').then(accessToken => {
      this.accessToken = accessToken || undefined;
    });
  }

  public isLoggedIn() {
    if (this.accessToken) return true;
    return false;
  }


  public extractData = (res: Response) => {
    this.events.publish('loader:hide');
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    this.saveHeaders(res.headers);

    let body = "";

    if(res){
        body = res.json();
    }
    this.cache[res.url] = body;
    return body || {};
  }

  public saveHeaders = (headers: any) => {
    if (headers && headers.get('Access-Token')) {
      this.accessToken = headers.get('Access-Token');
      this.storage.set('accessToken', headers.get('Access-Token')).then((data1) => {
        this.initializeTokens().then(resp => resp);
      });
    } else {
      this.initializeTokens().then(resp => resp);
    }
  }

  public handleError = (error: any): Observable < any > => {
   this.events.publish('loader:hide');

   this.saveHeaders(error.headers);
   let errMsg: any = {};
   if (error._body && typeof error._body !== 'object') errMsg = JSON.parse(error._body);
   errMsg.status = error.status;
   this.events.publish('error', errMsg);
   return Observable.throw(errMsg);
 }



}
