import { Injectable } from '@angular/core';

interface SubscribeFunc {
  (topic: 'toast:success' | 'toast:error' | 'toast:warning' | 'loader:show' | 'loader:hide' |'logout' | 'error' , fn?: any): void;
}

@Injectable()
export class EventsService {

  public topics: any = {}; // {'toast':[fn1], 'loading':[fn2]}
  public debug: boolean = false;

  constructor() { }


  subscribe: SubscribeFunc = (topic, fn) => {
    if (!this.topics[topic]) this.topics[topic] = [];
    this.topics[topic].push(fn);
    this.log();
  }

  unsubscribe: SubscribeFunc = (topic, fn) => {
    if (this.topics[topic]) {
      var index = this.topics[topic].indexOf(fn);
      if (index !== -1) this.topics[topic].splice(index, 1);
    }
    this.log();
  }

  publish: SubscribeFunc = (topic, value?) => {
    if (this.topics[topic]) {
      this.topics[topic].forEach(fn => {
        fn.call(this, value);
      });
    }
    this.log();
  }

  log() {
    if (this.debug) {

    }
  }
}
